#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdatomic.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>

#ifdef DEBUG
#define debug(...) { fprintf (stderr, __VA_ARGS__); fflush(stderr); }
#else
#define debug(...)
#endif

#define S 8
#define R 2

enum error_code {
	SUCCESS = 0,
	// TODO
	NOT_IMPLEMENTED,
};

typedef struct pthread_channel {
	// TODO
} pchannel_t;


int pchannel_init(pchannel_t *chan) {
	// TODO
	return NOT_IMPLEMENTED;
}

int pchannel_send(pchannel_t *chan, uintptr_t value) {
	// TODO
	return NOT_IMPLEMENTED;
}

int pchannel_recv(pchannel_t *chan, uintptr_t *value) {
	// TODO
	return NOT_IMPLEMENTED;
}

int pchannel_close(pchannel_t *chan) {
	// TODO
	return NOT_IMPLEMENTED;
}

pchannel_t chan;
atomic_int wg;

void *worker(void* arg) {
	int k = (int)arg;
	for (int i = 0; i < (1<<10); ++i)
		pchannel_send(&chan, (uintptr_t)S*i + k);
	if (atomic_fetch_sub(&wg, 1) == 1) {
		pchannel_close(&chan);
		debug("close in thread %d\n", k);
	}
	debug("thread %d exit\n", k);
	return NULL;
}

void *listener(void *arg) {
	FILE* f = (FILE*)arg;
	while (atomic_load(&wg)) {
		uintptr_t val;
		if (pchannel_recv(&chan, &val) == SUCCESS)
			fprintf(f, "%lu\n", val);
	}
	return NULL;
}

int main() {
	atomic_store(&wg, S);
	pthread_t senders[S];
	pthread_t listeners[R];
	FILE *fs[R];
	char buf[32];
	pchannel_init(&chan);

	for (int i = 0; i < R; ++i) {
		snprintf(buf, sizeof(buf), "log_%d", i);
		fs[i] = fopen(buf, "w");
		if (fs[i] == NULL) {
			fprintf(stderr, "Failed to open file '%s': %s", buf, strerror(errno));
			exit(1);
		}
		pthread_create(listeners + i, NULL, listener, (void*)fs[i]);
	}

	for (int i = 0; i < S; ++i)
		pthread_create(senders + i, NULL, worker, (void*)i);

	void *retval;
	for (int i = 0; i < S; ++i)
		pthread_join(senders[i], &retval);
	for (int i = 0; i < R; ++i) {
		pthread_join(listeners[i], &retval);
		fclose(fs[i]);
	}

	return 0;
}
